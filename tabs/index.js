const tabs = document.querySelectorAll('.tabs-title')
const tabsContent = document.querySelectorAll('.tabs-item')

tabs.forEach((tab) => {
  tab.addEventListener('click', () => {
    let currentTab = tab
    let tabAttribute = currentTab.getAttribute('data-tab')
    let currentContent = document.querySelector(tabAttribute)


    if (!currentTab.classList.contains('active')) {
      tabs.forEach((item) => {
        item.classList.remove('active')
      })
  
      tabsContent.forEach((item) => {
        item.classList.remove('active')
      })
  
      
      currentTab.classList.add('active')
      currentContent.classList.add('active')
    }

  })
})

document.querySelector('.tabs-title').click()
